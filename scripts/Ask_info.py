#!/usr/bin/env python
import rospy, os, sys

from sound_play.libsoundplay import SoundClient
from std_msgs.msg import String

def sleep(t):
    try:
        rospy.sleep(t)
    except:
        pass
def callback(msg1):
    rospy.loginfo(msg1.data)
    soundhandle.say(msg1.data)



if __name__ == '__main__':
    rospy.init_node('soundplay_test', anonymous = True)
    soundhandle = SoundClient()
    rospy.sleep(1)
    soundhandle.stopAll()
    print "Begin the receptionist mdoel."
    sleep(1)
    print "Ask for information."
    soundhandle.say('May I have your information please ?')
    sleep(3)
    print "Waiting for information"
    rospy.Subscriber("pass_info", String, callback)
    rospy.spin()
