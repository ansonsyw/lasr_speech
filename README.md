# lasr_speech

ROS package to record the information from speech and also could get the robot to speak the information it collected.

1. Installation

    (1).Install ROS sound_play
    
          ■ $ sudo apt-get install ros-<version of ROS>-audio-common
          
          ■ $ sudo apt-get install libasound2
          
          Tutorials: http://wiki.ros.org/sound_play/Tutorials
          
    (2).Install pocketsphinx
    
          ■ $ sudo apt-get install python-pip python-dev build-essential
          
          ■ $ sudo pip install --upgrade pip
          
          ■ $ sudo apt-get install libasound-dev
          
          ■ $ sudo apt-get install python-pyaudio
          
          ■ $ sudo pip install pyaudio
          
          ■ $ sudo apt-get install swig
          
          ■ $ sudo pip install pocketsphinx
          
    (3).Install ROS package for Pocketsphinx
    
          ■ $ cd ~/catkin_ws/src
          
          ■ $ git clone https://github.com/Pankaj-Baranwal/pocketsphinx
          
          ■ $ cd ~/catkin_ws
          
          ■ $ catkin_make
          
    (4).Add language model
    
          ■ Download and copy the content of hub4wsj_sc_8k language model to
            /usr/local/share/pocketsphinx/model/en-us/en-us/
            
          ■ https://sourceforge.net/projects/cmusphinx/files/Acoustic%20and%20Language%20Mod
          els/Archive/US%20English%20HUB4WSJ%20Acoustic%20Model/

2.Speech launch files

Before this, make sure all the files in the pkg are executable

    chmod +x <filename>
    
Using the following file to record information from speech. Change the path of the .dict and .lm file to your own.

    * Speaker.launch

Using the following file to run the speech server.

    * speech.launch
    
Then you should hearing the robot ask you to choose a mode,use following keywords:
    
    (1)Reception
    
    (2)Find my mate(Not done)
    
    (3)Take my bag(Not done)

Reception:
  
  After the terminal shows detecting people, do:
       
        rosrun reception client.py

  After hearing the question from robot, you could use sentences contains the following key words as answers in 15 seconds
    
    (1). Andy/Lisa
    
    (2). Coke/Juice

Example:

    (1). My name is Andy./I'm Lisa.
    
    (2). My favourite drink is coke./I like juice.
    
Then, say "That's all" to send information to client.
    

3.If you want to Change the Vocabulary to your own

    (1) $ roscd lasr_speech/dict
    
    (2) $ nano receptionist.corpus (Other compiler is okay)
    
    (3) http://www.speech.cs.cmu.edu/tools/lmtool-new.html
    
    (4) Update dic and lm files.

4.References

   Robot speech from RoboCup@Home  https://www.robocupathomeedu.org/challenges/robocuphome-education-online-challenge-2020 by Jeffrey Tan 
