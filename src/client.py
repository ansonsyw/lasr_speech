#! /usr/bin/env python

import rospy
import actionlib
import receptionist.msg


#from sound_play.libsoundplay import SoundClient
from std_msgs.msg import String
my_data = None
def reception_client():
    client = actionlib.SimpleActionClient('receptionist', receptionist.msg.informationAction)
    rospy.init_node('receptionist_client')
    client.wait_for_server()
    my_data = "Person"
    goal = receptionist.msg.informationGoal(my_data)
    client.send_goal(goal)
    client.wait_for_result()
    result = client.get_result()
    print(result)
    rospy.spin()


if __name__ == '__main__':
    reception_client()
