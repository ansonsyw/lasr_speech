#! /usr/bin/env python

import rospy
import actionlib
import receptionist.msg

from std_msgs.msg import String
from sound_play.libsoundplay import SoundClient
soundhandle = SoundClient()

def callback(detected_info):
    rospy.loginfo(detected_info.data)
    if detected_info.data.find("RECEPTION") > -1:
        print("Starting receptionist mode")
        soundhandle.say("Reception mode start")
        sub.unregister()
        Receptionist()
    if detected_info.data.find("FIND MY MATE") > -1:
        print("Starting Find my mate mode")
        soundhandle.say("Starting Find my mate mode")
        sub.unregister()
        print("Find my mate mode started")
    if detected_info.data.find("MY BAG") > -1:
        print("Starting Take my bag mode")
        soundhandle.say("Starting Take my bag mode")
        sub.unregister()
        print("Take my bag mode started")


class Receptionist(object):
    def __init__(self):
        self._result = receptionist.msg.informationResult()
        self.name = []
        self.drink = []
        print("Detecting people...")
        self.server = actionlib.SimpleActionServer('receptionist', receptionist.msg.informationAction, execute_cb=self.execute_cb, auto_start=False)
        self.server.start()

    def getinfo(self, detected_info1):
        rospy.loginfo(detected_info1.data)
        if detected_info1.data.find("ANDY") > -1:
            self.name = "Andy"
        elif detected_info1.data.find("LISA") > -1:
            self.name = "Lisa"
        elif detected_info1.data.find("COKE") > -1:
            self.drink = "Coke"
        elif detected_info1.data.find("JUICE") > -1:
            self.drink = "Juice"
        elif detected_info1.data.find("THAT'S ALL") > -1:
            self._result.name = self.name
            self._result.drink = self.drink




    def execute_cb(self, goal):
        print("Person No.1 detected!")
        print("Ask for information")
        soundhandle.say("May I have your information please?")
        rospy.Subscriber("lm_data", String, self.getinfo)
        rospy.sleep(15)
        self.server.set_succeeded(self._result)
        print("Information successfully passed to client!")







if __name__ == '__main__':
    rospy.init_node('reception_server')
    print("Waiting for a command")
    soundhandle.say("Please choose a mode")
    sub = rospy.Subscriber("lm_data", String, callback)
    rospy.spin()
